/*
 * Copyright (c) 2015, CloudSherpas Incorporated. All rights reserved.
 */
package com.cloudsherpas.courseadvisor.utils;

public final class DroolsConfigConstants {

    private DroolsConfigConstants() {}

    public static final String WB_USERNAME = "admin";
    public static final String WB_PASSWORD = "admin";
    public static final String WB_URL = "http://localhost:8080/drools-wb/";
    public static final String WB_REPO_PATH = "workbench.repo.path";
    public static final String WB_ARTIFACT_PATH = "workbench.repo.artifact.path";
    public static final String WB_ARTIFACT_NAME = "workbench.repo.artifact.name";
}
